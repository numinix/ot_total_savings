<?php
/**
 * @package order_total
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_shipping_discounts.php 3 2008-07-04 04:45:38Z numinix $
 */
 
  define('MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_TITLE', 'Total Discounts');
  define('MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_DESCRIPTION', 'Total Discounts');
  
?>