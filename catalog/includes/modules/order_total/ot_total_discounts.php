<?php
/**
 * @package order_total
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_shipping_discounts.php 9 2008-08-01 02:28:05Z numinix $
 */
 
class ot_total_discounts {
  var $code, $title, $description, $enabled, $sort_order, $included_modules, $modules, $output;
  
  function ot_total_discounts() {
    //global $order_total;
    $this->code = 'ot_total_discounts';
    $this->title = MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_TITLE;
    $this->description = MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_DESCRIPTION;
    $this->enabled = (MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_STATUS == 'true' ? true : false);
    $this->sort_order = MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_SORT_ORDER;
    if (!IS_ADMIN_FLAG && $this->enabled) {
      $this->modules = array();
      $this->included_modules = split(',', trim(MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_MODULES));
      $this->output = array();
      $this->total_discounts = $_SESSION['total_discounts'];
    }
  }
  
  function process() {
    global $currencies;
    
    if ($this->enabled && $this->total_discounts) {
      $this->output[] = array('title' => $this->title . ':',
                              'text' => '-' . $currencies->format($this->total_discounts),
                              'value' => $this->total_discounts); 
    }     
  }
  
  function total_discount() {
    global $currencies;

    if (is_array($this->modules)) {
      reset($this->modules);
      $total_discount = 0;
      while (list(, $value) = each($this->modules)) {
        if (in_array(substr($value, 0, -4), $this->included_modules)) {
          $class = substr($value, 0, strrpos($value, '.'));
          $size = sizeof($GLOBALS[$class]->output);
          echo $size;
          die();
          for ($i=0; $i<$size; $i++) { 
            $amount = substr(html_entity_decode($GLOBALS[$class]->output[$i]['text']), 2);
            $total_discount += $amount;
          }
        }
      }
      return $total_discount;
    } else {
      return false;
    }
  }
  
  function check() {
    global $db;
    if (!isset($this->check)) {
      $check_query = $db->Execute("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_STATUS'");
      $this->check = $check_query->RecordCount();
    }
    return $this->check;
  }
  
  function keys() {
    $keys = array('MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_STATUS', 'MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_SORT_ORDER', 'MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_MODULES');
    return $keys;
  }
  
  function install() {
    global $db;
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Activate', 'MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_STATUS', 'true', 'Do you want to enable Total Discounts?', '6', '1','zen_cfg_select_option(array(\'true\', \'false\'), ', now())");     
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Discount Modules List', 'MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_MODULES', 'ot_shipping_discount,ot_per_category', 'List of enabled discount modules:', '6', '2', 'zen_cfg_textarea(', now())");    
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_ORDER_TOTAL_TOTAL_DISCOUNTS_SORT_ORDER', '998', 'Sort order of display.', '6', '99', now())");
  }
  
  function remove() {
    global $db;
    $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
  }  
}
  